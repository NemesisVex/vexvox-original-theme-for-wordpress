# VexVox Original Theme

A custom theme for [VexVox](http://gregbueno.com/wp/vexvox/).

Dependencies
------------

* GruntJS

Plugin suport
-------------

* Movable Type ID Mapper

## Implementations

### Bootstrap Overrides for WordPress

`TemplateTags::paging_nav()` checks whether the Bootstrap Overrides plugin is enabled. If so,
it uses `Setup::wp_page_menu_args()` to pass arguments to the overridden paging functions.
See the [plugin documentation](https://bitbucket.org/NemesisVex/bootstrap-overrides-for-wordpress)
for usage.

## Child Themes

* [Meisakuki Original Theme](https://bitbucket.org/NemesisVex/meisakuki-original-theme-for-wordpress)
* [Sakufu Original Theme](https://bitbucket.org/NemesisVex/sakufu-original-theme-for-wordpress)
